package com.example.anfibios

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_caracteristicas.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_menu.*

class menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        regresar.setOnClickListener { val regreso: Intent =  Intent(this, MainActivity::class.java)
        startActivity(regreso)
        }

        def.setOnClickListener { val definicion:Intent = Intent(this, conceptos ::class.java)
        startActivity(definicion)
        }

        imageButton6.setOnClickListener { val back: Intent = Intent(this, caracteristicas::class.java)
        startActivity(back)
        }

        imageButton8.setOnClickListener { val ira:Intent = Intent(this, taxonomia::class.java)
        startActivity(ira)
        }

        imageButton7.setOnClickListener { val inte:Intent= Intent(this, apodo::class.java)
        startActivity(inte)
        }

        imageButton9.setOnClickListener { val rel:Intent = Intent(this, ranas::class.java)
        startActivity(rel)
        }

        imageButton10.setOnClickListener { val vas:Intent = Intent(this, salamandras::class.java)
        startActivity(vas)
        }
    }
}